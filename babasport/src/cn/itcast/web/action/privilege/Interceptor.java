package cn.itcast.web.action.privilege;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

//@Aspect @Component//标注不好划分层面的时候用该注解让其被spring管理
public class Interceptor {
//	@Pointcut("execution(org.apache.struts.action.ActionForward cn.itcast.web.action..*.*(org.apache.struts.action.ActionMapping,org.apache.struts.action.ActionForm,javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse))")
//	public void actionMethod(){	} //切入点的名称
	
//	"execution(* com.cdfahe.gms.service.impl.*.*(..)) "
	@Pointcut("execution(org.apache.struts.action.ActionForward cn.itcast.web.action..*.*(..))")
	public void actionMethod(){	}
	
	@Around("actionMethod()")
	public Object interceptor(ProceedingJoinPoint pjp)throws Throwable{
		System.out.println("拦截到了" + pjp.getSignature().getName() +"方法11111111111111");
		
//		if (session.getAttribute("employee").getPrivage().contains("xx权限")) {
//			return pjp.proceed();
//		}else{
//			return("提示无权限视图");
//		}
		
		return pjp.proceed();
	}
	
	
}
