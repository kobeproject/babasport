package cn.itcast.web.action.privilege;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 权限配置
 */
@Retention(RetentionPolicy.RUNTIME) //配置注解保留的阶段
@Target(ElementType.METHOD)			//配置可以标注在的地方
public @interface Permission {
	/* 模块名 */
	String model();
	/* 权限值 */
	String privilegeValue();
}
